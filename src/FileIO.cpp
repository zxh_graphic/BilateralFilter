/** @file FileIO.cpp
 * @brief file defining methods to read points from a file
 * @author Julie Digne julie.digne@liris.cnrs.fr
 * @date 2012/10/22
 * @copyright This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "FileIO.h"
#include<iostream>
#include<fstream>
#include<istream>
#include<ostream>
#include<sstream>
#include<algorithm>
#include<deque>
#include<limits>

using namespace std;



FileIO::FileIO()
{
}

FileIO::~FileIO()
{
}

double FileIO::estimateRadius(const char* filename)
{
    ifstream in;
    in.open(filename);

    if(!in)
    {
        std::cerr<<"File "<<filename<<" could not be opened"<<std::endl;
        return false;
    }

    string line;

    getline(in, line);

    double x,y,z;
    in >> x >> y >> z;

    double xmin, ymin, zmin, xmax, ymax, zmax;
    xmin = xmax = x;
    ymin = ymax = y;
    zmin = zmax = z;

    unsigned int npts = 1;
    while(getline(in,line))
    {
        in >> x >> y >> z;
        xmin = std::min(x,xmin);
        xmax = std::max(x,xmax);
        ymin = std::min(y,ymin);
        ymax = std::max(y,ymax);
        zmin = std::min(z,zmin);
        zmax = std::max(z,zmax);
        ++npts;
    }
    in.close();

    double lx = xmax - xmin;
    double ly = ymax - ymin;
    double lz = zmax - zmin;

    double size = std::max(lx,ly);
    size = std::max(lz,size);

    return std::sqrt(20.0/(double)npts)*size;
}

bool FileIO::readAndSortPCLPoints(std::string filename, Octree &octree,
                                       double min_radius)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr P(new pcl::PointCloud<pcl::PointXYZ> ());
    pcl::io::loadPLYFile(filename, *P);

    // Create the normal estimation class, and pass the input dataset to it
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
    ne.setInputCloud(P);

    // Create an empty kdtree representation, and pass it to the normal estimation object.
    // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
    ne.setSearchMethod (tree);
    // Output datasets
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

    // Use all neighbors in a sphere of radius 3cm
    ne.setRadiusSearch (min_radius);
    // Compute the features
    ne.compute (*cloud_normals);

    deque<Sample> input_vertices;


    double xmin, ymin, zmin, xmax, ymax, zmax;
    xmin = xmax = static_cast<double>(P->points[0].x);
    ymin = ymax = static_cast<double>(P->points[0].y);
    zmin = zmax = static_cast<double>(P->points[0].z);

    for(size_t i = 0; i < P->points.size(); i++)
    {
        Sample s(static_cast<double>(P->points[i].x),
                 static_cast<double>(P->points[i].y),
                 static_cast<double>(P->points[i].z));
        s.setIndex(i);
        input_vertices.push_back(s);

        xmin = std::min(static_cast<double>(P->points[i].x), xmin);
        xmax = std::max(static_cast<double>(P->points[i].x), xmax);
        ymin = std::min(static_cast<double>(P->points[i].y), ymin);
        ymax = std::max(static_cast<double>(P->points[i].y), ymax);
        zmin = std::min(static_cast<double>(P->points[i].z), zmin);
        zmax = std::max(static_cast<double>(P->points[i].z), zmax);

        octree.addProperty(3);
        octree.setProperty(i, 0, static_cast<double>(cloud_normals->points[i]._Normal::normal_x));
        octree.setProperty(i, 1, static_cast<double>(cloud_normals->points[i]._Normal::normal_y));
        octree.setProperty(i, 2, static_cast<double>(cloud_normals->points[i]._Normal::normal_z));
    }
    std::cout<<input_vertices.size()<<" points read"<<std::endl;

    double lx = xmax - xmin;
    double ly = ymax - ymin;
    double lz = zmax - zmin;

    double size = std::max(lx,ly);
    size = std::max(lz,size);
    size = 1.1 * size;//loose bounding box around the object

    double margin;
    if(min_radius > 0)
    {
        unsigned int depth = (unsigned int)ceil( log2( size / (min_radius) ));
        //adapting the bouding box size so that the smallest cell size is
        //exactly 2*min_radius
        double adapted_size = pow2(depth) * min_radius;
        //margins of the bouding box around the object
        margin = 0.5 * (adapted_size - size);
        size = adapted_size;
        octree.setDepth(depth);
    }
    else
    {
        margin = 0.05 * size; //margins of the bouding box around the object
    }

    double ox = xmin - margin;
    double oy = ymin - margin;
    double oz = zmin - margin;
    Point origin(ox,oy,oz);

    octree.initialize(origin, size);

    //add the points to the set with index 0 (initial set)
    octree.addInitialPoints(input_vertices.begin(), input_vertices.end());

    return true;
}

bool FileIO::readAndSortPCLPolygon(std::string filename, Octree &octree,
                                   double min_radius)
{
    pcl::PolygonMesh mesh;
    pcl::io::loadPLYFile(filename,mesh);


    pcl::PointCloud<pcl::PointXYZ>::Ptr P(new pcl::PointCloud<pcl::PointXYZ> ());
    pcl::fromPCLPointCloud2(mesh.cloud, *P);

    // Create the normal estimation class, and pass the input dataset to it
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
    ne.setInputCloud(P);

    // Create an empty kdtree representation, and pass it to the normal estimation object.
    // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
    ne.setSearchMethod (tree);
    // Output datasets
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

    // Use all neighbors in a sphere of radius 3cm
    ne.setRadiusSearch (min_radius);
    // Compute the features
    ne.compute (*cloud_normals);

    deque<Sample> input_vertices;


    double xmin, ymin, zmin, xmax, ymax, zmax;
    xmin = xmax = static_cast<double>(P->points[0].x);
    ymin = ymax = static_cast<double>(P->points[0].y);
    zmin = zmax = static_cast<double>(P->points[0].z);

    for(size_t i = 0; i < P->points.size(); i++)
    {
        Sample s(static_cast<double>(P->points[i].x),
                 static_cast<double>(P->points[i].y),
                 static_cast<double>(P->points[i].z));
        s.setIndex(i);
        input_vertices.push_back(s);

        xmin = std::min(static_cast<double>(P->points[i].x), xmin);
        xmax = std::max(static_cast<double>(P->points[i].x), xmax);
        ymin = std::min(static_cast<double>(P->points[i].y), ymin);
        ymax = std::max(static_cast<double>(P->points[i].y), ymax);
        zmin = std::min(static_cast<double>(P->points[i].z), zmin);
        zmax = std::max(static_cast<double>(P->points[i].z), zmax);

        octree.addProperty(3);
        octree.setProperty(i, 0, static_cast<double>(cloud_normals->points[i]._Normal::normal_x));
        octree.setProperty(i, 1, static_cast<double>(cloud_normals->points[i]._Normal::normal_y));
        octree.setProperty(i, 2, static_cast<double>(cloud_normals->points[i]._Normal::normal_z));
    }
    std::cout<<input_vertices.size()<<" points read"<<std::endl;

    double lx = xmax - xmin;
    double ly = ymax - ymin;
    double lz = zmax - zmin;

    double size = std::max(lx,ly);
    size = std::max(lz,size);
    size = 1.1 * size;//loose bounding box around the object

    double margin;
    if(min_radius > 0)
    {
        unsigned int depth = (unsigned int)ceil( log2( size / (min_radius) ));
        //adapting the bouding box size so that the smallest cell size is
        //exactly 2*min_radius
        double adapted_size = pow2(depth) * min_radius;
        //margins of the bouding box around the object
        margin = 0.5 * (adapted_size - size);
        size = adapted_size;
        octree.setDepth(depth);
    }
    else
    {
        margin = 0.05 * size; //margins of the bouding box around the object
    }

    double ox = xmin - margin;
    double oy = ymin - margin;
    double oz = zmin - margin;
    Point origin(ox,oy,oz);

    octree.initialize(origin, size);

    //add the points to the set with index 0 (initial set)
    octree.addInitialPoints(input_vertices.begin(), input_vertices.end());

    return true;
}

bool FileIO::readAndSortUnorientedPoints(const char* filename,
                               Octree& octree,
                               double min_radius)
{
    ifstream in;
    in.open(filename);

    if(!in)
    {
        std::cerr<<"File "<<filename<<" could not be opened"<<std::endl;
        return false;
    }

    string firstline;
    getline(in, firstline);

    istringstream line_in(firstline);
    string word;
    int nword = 0;
    while (line_in >> word)
        nword++;

    if(nword < 3)
    {
        cerr<< "File must contain at least 3 doubles per line (x y z)"<<endl;
        return false;
    }

    size_t nprop = nword - 3;
    std::cout<<"nprop = "<<nprop<<std::endl;

    std::cout<<"Unoriented points with "<<nprop<<" properties"<<std::endl;

    in.clear() ;
    in.seekg(0, ios::beg);//starting back from the beginning

    double x,y,z;
    in >> x >> y >> z;

    deque<Sample> input_vertices;
    Sample s(x,y,z);
    s.setIndex(0);
    input_vertices.push_back(s);

    octree.addProperty(nprop);

    for(size_t t = 0 ; t < nprop ; ++t)
    {
      double val;
      in >> val;
      octree.setProperty(0, t, val);
    }

    double xmin, ymin, zmin, xmax, ymax, zmax;
    xmin = xmax = x;
    ymin = ymax = y;
    zmin = zmax = z;

    size_t index = 1;
    while( in >> x >> y >> z)
    {
      Sample temp(x,y,z);
      temp.setIndex(index);
      input_vertices.push_back(temp);
      xmin = std::min(x, xmin);
      xmax = std::max(x, xmax);
      ymin = std::min(y, ymin);
      ymax = std::max(y, ymax);
      zmin = std::min(z, zmin);
      zmax = std::max(z, zmax);
    
    //reading properties
      octree.addProperty(nprop);
      for(size_t t = 0; t < nprop; ++t)
      {
        double val;
        in >> val;
        octree.setProperty(index, t, val);
      }
      ++index;
    }
    in.close();

    std::cout<<input_vertices.size()<<" points read"<<std::endl;

    double lx = xmax - xmin;
    double ly = ymax - ymin;
    double lz = zmax - zmin;

    double size = std::max(lx,ly);
    size = std::max(lz,size);
    size = 1.1 * size;//loose bounding box around the object

    double margin;
    if(min_radius > 0)
    {
        unsigned int depth = (unsigned int)ceil( log2( size / (min_radius) ));
        //adapting the bouding box size so that the smallest cell size is 
        //exactly 2*min_radius
        double adapted_size = pow2(depth) * min_radius;
        //margins of the bouding box around the object
        margin = 0.5 * (adapted_size - size); 
        size = adapted_size;
        octree.setDepth(depth);
    }
    else
    {
        margin = 0.05 * size; //margins of the bouding box around the object
    }

    double ox = xmin - margin;
    double oy = ymin - margin;
    double oz = zmin - margin;
    Point origin(ox,oy,oz);

    octree.initialize(origin, size);

    //add the points to the set with index 0 (initial set)
    octree.addInitialPoints(input_vertices.begin(), input_vertices.end());

    return true;
}

bool FileIO::readAndSortOrientedPoints(const char* filename,
                               Octree& octree,
                               double min_radius)
{
    ifstream in;
    in.open(filename);

    if(!in)
    {
        std::cerr<<"File "<<filename<<" could not be opened"<<std::endl;
        return false;
    }

    string firstline;
    getline(in, firstline);

    istringstream line_in(firstline);
    string word;
    int nword = 0;
    while (line_in>> word)
        nword++;

    if( nword < 6)
    {
        cerr<<"each point must be given by at least 6 values (position + normal) : "
        <<"x y z nx ny nz"<<endl;
        return false;
    }
    
    size_t nprop = nword - 6;
        std::cout<<"nprop = "<<nprop<<std::endl;
    std::cout<<"Oriented points with "<<nprop<<" properties"<<std::endl;

    in.clear() ;
    in.seekg(0, ios::beg);//starting back from the beginning

    double x,y,z,nx,ny,nz;
    in >> x >> y >> z >> nx >> ny >> nz;
    Sample s(x, y, z, nx, ny, nz);
    s.setIndex(0);
    deque<Sample> input_vertices;
    input_vertices.push_back(s);

    octree.addProperty(nprop);
    for(size_t t = 0 ; t < nprop ; ++t)
    {
      double val;
      in >> val;
      octree.setProperty(0, t, val);
    }

    double xmin, ymin, zmin, xmax, ymax, zmax;
    xmin = xmax = x;
    ymin = ymax = y;
    zmin = zmax = z;

    size_t index = 1;
    while( in >> x >> y >> z >> nx >> ny >> nz)
    {
        Sample temp(x,y,z,nx,ny,nz);
        temp.setIndex(index);
        input_vertices.push_back(temp);
        xmin = std::min(x,xmin);
        xmax = std::max(x,xmax);
        ymin = std::min(y,ymin);
        ymax = std::max(y,ymax);
        zmin = std::min(z,zmin);
        zmax = std::max(z,zmax);

        octree.addProperty(nprop);
        for(size_t t = 0 ; t < nprop ; ++t)
        {
          double val;
          in >> val;
          octree.setProperty(index, t, val);
        }

        ++index;
    }
    in.close();

    std::cout<<input_vertices.size()<<" points read"<<std::endl;

    double lx = xmax - xmin;
    double ly = ymax - ymin;
    double lz = zmax - zmin;

    double size = std::max(lx,ly);
    size = std::max(lz,size);
    size = 1.1 * size;//loose bounding box around the object

    double margin;
    if(min_radius > 0)
    {
        unsigned int depth = (unsigned int)ceil( log2( size / (min_radius) ));
        //adapting the bouding box size so that the smallest cell size is 
        //exactly 2*min_radius
        double adapted_size = pow2(depth) * min_radius;
        //margins of the bouding box around the object
        margin = 0.5 * (adapted_size - size); 
        size = adapted_size;
        octree.setDepth(depth);
    }
    else
    {
        margin = 0.05 * size; //margins of the bouding box around the object
    }

    double ox = xmin - margin;
    double oy = ymin - margin;
    double oz = zmin - margin;
    Point origin(ox,oy,oz);

    octree.initialize(origin, size);

    //add the points to the set with index 0 (initial set)
    octree.addInitialPoints(input_vertices.begin(), input_vertices.end());

    return true;
}


bool FileIO::savePointsPCL(std::string filename, Octree &octree,
                             unsigned int index, bool isoriented)
{
    std::cout.precision(numeric_limits<double>::digits10 + 1);
    std::vector<double3> points;

    OctreeNode *node = octree.getRoot();
    saveContentPCL(node, octree, index, points, isoriented);

    std::cout<<"points.size() = "<<points.size()<<std::endl;

    if(points.size() == 0)
        return false;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>());
    cloud->width = points.size();
    cloud->height = 1;
    cloud->is_dense = true;

    cloud->points.resize(cloud->width * cloud->height);

    for(size_t i = 0; i < points.size(); i++)
    {
        cloud->points[i].x = static_cast<float>(points[i].x);
        cloud->points[i].y = static_cast<float>(points[i].y);
        cloud->points[i].z = static_cast<float>(points[i].z);
    }

    pcl::PLYWriter writer;
    writer.write(filename, *cloud);

    return true;
}


bool FileIO::savePolygonPCL(std::string filenameOpen,
                            std::string filenameSave,
                            Octree &octree,
                             unsigned int index, bool isoriented)
{
    pcl::PolygonMesh mesh;
    pcl::io::loadPLYFile(filenameOpen,mesh);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>());
    pcl::fromPCLPointCloud2(mesh.cloud, *cloud);

    std::cout.precision(numeric_limits<double>::digits10 + 1);
    std::vector<double3> points;

    OctreeNode *node = octree.getRoot();
    saveContentPCL(node, octree, index, points, isoriented);

    std::cout<<"points.size() = "<<points.size()<<std::endl;

    if(points.size() == 0)
        return false;


    for(size_t i = 0; i < points.size(); i++)
    {
        cloud->points[i].x = static_cast<float>(points[i].x);
        cloud->points[i].y = static_cast<float>(points[i].y);
        cloud->points[i].z = static_cast<float>(points[i].z);
    }

    pcl::toPCLPointCloud2(*cloud, mesh.cloud);

    pcl::io::savePLYFile(filenameSave, mesh);

    return true;

}

void FileIO::saveContentPCL(OctreeNode *node, Octree &octree, unsigned int index,
                            std::vector<double3>& pointsave, bool isoriented)
{
    if(node->getDepth() != 0)
    {
        for(int i = 0; i < 8 ;i++)
            if(node->getChild(i) != NULL)
                saveContentPCL(node->getChild(i), octree, index, pointsave, isoriented);
    }
    else if(node->getNpts(index) != 0)
    {
        Sample_deque::const_iterator iter;
        std::vector<double>::iterator pi;
        for(iter = node->points_begin(index);
            iter != node->points_end(index);++iter)
        {
            const Sample &s = *iter;
            size_t sindex = s.index();
            if(isoriented)
            {
                double3 temp;
                temp.x = s.x();
                temp.y = s.y();
                temp.z = s.z();
                pointsave.push_back(temp);
                //f << s;
            }
            else
            {
                double3 temp;
                temp.x = s.x();
                temp.y = s.y();
                temp.z = s.z();
                pointsave.push_back(temp);
                //f << s.x() << "\t" << s.y() << "\t" << s.z();
            }
            if(octree.getNproperties() > 0)
            {
                //f<<"\t";
                for(size_t pindex = 0; pindex < octree.getNproperties(); ++pindex)
                {
                    double val = octree.getProperty(sindex, pindex);
                    //f<<val<<"\t";
                }
            }
            //f<<endl;
        }
    }
}


bool FileIO::savePoints(const char* filename, Octree& octree,
                        unsigned int index, bool isoriented)
{ 
    ofstream out;
    out.open(filename);
    out.precision( numeric_limits<double>::digits10 + 1);

    if(!out)
        return false;

    OctreeNode *node = octree.getRoot();
    saveContent(node, octree, index, out, isoriented);

    out.close();

    return true;
}


void FileIO::saveContent(OctreeNode* node, Octree &octree, unsigned int index,
                         ofstream& f, bool isoriented)
{
    if(node->getDepth() != 0)
    {
        for(int i = 0; i < 8 ;i++)
            if(node->getChild(i) != NULL)
                saveContent(node->getChild(i), octree, index, f, isoriented);
    }
    else if(node->getNpts(index) != 0)
    {
        Sample_deque::const_iterator iter;
        std::vector<double>::iterator pi;
        for(iter = node->points_begin(index);
            iter != node->points_end(index);++iter)
        {
            const Sample &s = *iter;
            size_t sindex = s.index();
            if(isoriented)
                f << s;
            else
                f << s.x() << "\t" << s.y() << "\t" << s.z();
            if(octree.getNproperties() > 0)
            {
                f<<"\t";
                for(size_t pindex = 0; pindex < octree.getNproperties(); ++pindex)
                {
                    double val = octree.getProperty(sindex, pindex);
                    f<<val<<"\t";
                }
            }
            f<<endl;
        }
    }
}


